package ObjectClass;

public class P249 {
	static void go(Integer x){
		System.out.println("Integer:  " + x );
	}
	
	static void go(long x){
		System.out.println("Long : " + x);
	}
	
	public static void main(String... args){
		int i = 5;
		go(i);
	}
}
