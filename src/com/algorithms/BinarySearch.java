package com.algorithms;

import java.util.stream.IntStream;

public class BinarySearch {

	private static int[] generateArray() {
		int[] array = IntStream.iterate(30, i -> i + 1).limit(10).toArray();
		return array;
	}

	public static void main(String[] args) {
		int[] array = generateArray();
		int key = 38;
		System.out.println(binarySearch(key, array));
	}

	private static int binarySearch(int key, int[] array) {
		if (null == array) {
			return -1;
		}

		int low, high, mid;

		low = 0;

		high = array.length - 1;

		mid = (low + high) / 2;

		while (low < high) {
			if (array[mid] == key) {
				return mid;
			} else if (array[mid] > key) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
			mid = (low + high) / 2;
		}

		return -1;
	}

}
