package com.examples.enums;

enum CoffeeSize {
	BIG(8), HUGE(10), OVERWHELMING(16);
	
	CoffeeSize(int ounces) { // constructor
	this.ounces = ounces;
	}
	
	private int ounces; // an instance variable
	
	public int getOunces() {
	return ounces;
	}
	
	}


enum preIDStauts{
	MBP,MPO,SFP
}

class Coffee{
	CoffeeSize size;
}

public class EnumTest {
	public static void main(String[] args) {
		Coffee cf = new Coffee();
		cf.size = CoffeeSize.OVERWHELMING;
		
		System.out.println(cf.size.getOunces()); // prints 8
		
		
		for(CoffeeSize cs: CoffeeSize.values())
		System.out.println(cs + " " + cs.getOunces());
	}
}
