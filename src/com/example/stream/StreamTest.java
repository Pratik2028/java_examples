package com.example.stream;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import collection.Employee;
import collection.EmployeeUtils;

public class StreamTest {

	public static void main(String[] args) {
		
		Stream<String> nameStream = Stream.of("Pratik","Bhushan","Ajay","Prashant");
		//nameStream.forEach(System.out::println);
		Stream<Double> randoms = Stream.generate(Math::random).limit(5);
		//randoms.forEach(System.out::println);
		
		
		List<Employee> empList = EmployeeUtils.getEmployeeList();
		
		Map<Long, Employee> empMap 
					= empList.stream().collect(Collectors.toMap(Employee::getId, Function.identity(), (existingValue, newValue) -> existingValue)); 
		
		empMap.forEach((k,v) -> System.out.println(k + " : " + v ));
		
		
		//Grouping by Map
		Map<String,List<Employee>> empMapByRoleWise
			= empList.stream().collect(Collectors.groupingBy(Employee::getRole));
		System.out.println("");
		//System.out.println("groupingBy Partition : Role & Employee");
		empMapByRoleWise.forEach((k,v) -> System.out.println(k + " : " + v ));
		
		//Grouping By Partition
		Map<Boolean,List<Employee>> ManagerAndEmployess
		= empList.stream().collect(Collectors.partitioningBy(Employee::isManager));
		System.out.println("");
		//System.out.println("partitioningBy By Partition : ManagerAndEmployess");
		ManagerAndEmployess.forEach((k,v) -> System.out.println(k + " : " + v ));
	
		
	}

}
