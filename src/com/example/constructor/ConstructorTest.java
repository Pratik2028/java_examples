package com.example.constructor;

import java.util.function.Predicate;

class A {
	 A(){
		System.out.println("DC: A() Constructor");
	}
	A(String s){
		System.out.println("PC: A() Constructor " + s);
	}
}

class B extends A {
	public static final String name = "UNKNOW";
	public B(){
		this("acb");
		System.out.println("DC: B() Constructor ");
	}
	public B(String s){
		super();
		System.out.println("PC: B() Constructor " + s);
	}
}

public class ConstructorTest {
	public static void main(String[] args) {
		B b = new B();
	}

}
