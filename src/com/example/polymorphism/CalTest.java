package com.example.polymorphism;

class Square {
	String name = "name";
	public void calculate(double a) {
		System.out.println("Square of the given Number is " + a * a);
	}
	/*void calculate(int i){
		System.out.println("int Square of the given Number is " + i * i);
	}*/
}

 class Sqrt extends Square {
	String name = "name2";
	public void calculate(double a) {
		System.out.println("Square root of given number is: " + Math.sqrt(a));
	}
}

public class CalTest {

	public static void main(String[] args) {
		/*Square square = new Square();
		square.calculate(9);
		
		Sqrt sqrt = new Sqrt();
		sqrt.calculate(9);*/
		
		Square s = new Sqrt();
		s.calculate(9);
		System.out.println(s.name);
	}

}
