package com.example.exception;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


class BadNumberException extends Exception{
	BadNumberException(){
		
	}
	BadNumberException(String message){
		super(message);	
	}
}

class UserNameVaild extends RuntimeException{
	UserNameVaild(String message){
		super(message);
	}
}

class ThrowExeption{
	public static double divide(int numberToDivide, int numberToDivideBy) throws BadNumberException{
		 if(numberToDivideBy == 0){
	            throw new BadNumberException("Cannot divide by 0");
	        }
	        return numberToDivide / numberToDivideBy;
	}
	
	public static boolean isUserNameVaild(String username){
		if(username == null || username.isEmpty()){
			throw new UserNameVaild("Username is invalid! Plz Try again...");
		}
		return true;
	}
	
	private static void printFile() throws IOException {
	    InputStream input = null;
	    try 
	    {
	        input = new FileInputStream("file.txt");

	        int data = input.read();
	        while(data != -1){
	            System.out.print((char) data);
	            data = input.read();
	        }
	    } finally {
	        if(input != null){
	            input.close();
	        }
	    }
	}
}


public class ExceptionTest {
	public static void main(String[] args)  {
		System.out.println("Before Divide ");
		try {
			ThrowExeption.divide(100, 0);
		} catch (BadNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("After Divide ");
		ThrowExeption.isUserNameVaild("sdfsed");
		//sql exception
	}
}


