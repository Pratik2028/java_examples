package com.example.lambda;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import collection.Employee;
import collection.EmployeeUtils;

@FunctionalInterface
interface ConnectString {
	String connectToStrings(String s1, String s2);
	
	default String getDefaultComapnyName(){
		return "Ness Technolgies";
	}
}

public class LambdaExample implements ConnectString {
	/*
	 * A Java lambda expression is thus a function which can be created without
	 * belonging to any class.
	 */
	public static void main(String[] args) {

		//LambdaExpressionExample();

		//MethodRefenceExample();
		
		System.out.println(new LambdaExample().getDefaultComapnyName());
		System.out.println(new LambdaExample().connectToStrings("", ""));
	}

	private static void MethodRefenceExample() {
		// Method reference Examples :
		List<Employee> empList = EmployeeUtils.getEmployeeList();

		// without Method reference
		List<String> listWithoutMethodRefernces 
			= empList.stream()
				.map(emp -> emp.getName())
				.collect(Collectors.toList());

		// List With MR
		List<String> listWithMethodRefernces
			= empList.stream()
						.map(Employee::getName) // Class::instanceMethod
						.collect(Collectors.toList());
		
		System.out.println(listWithMethodRefernces);
	}

	private static void LambdaExpressionExample() {
		ConnectString anonymousClass = new ConnectString() {
			@Override
			public String connectToStrings(String s1, String s2) {
				return s1.concat(s2);
			}
		};

		System.out.println("anonymousClass : "
				+ anonymousClass.connectToStrings("Ness", "Technologies"));

		ConnectString lambdaExpressionString = (String s1, String s2) -> {
			return s1.concat(s2);
		};

		System.out.println("lambdaExpressionString : "
				+ lambdaExpressionString.connectToStrings("Ness",
						"Technologies"));

	}

	//Calling Interface Default Method
	public String connectToStrings(String s1, String s2) {
		// TODO Auto-generated method stub
		return ConnectString.super.getDefaultComapnyName();
	}
}
