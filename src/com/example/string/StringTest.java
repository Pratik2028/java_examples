package com.example.string;

public class StringTest {

	public static void main(String[] args) {
		String s = new String("Hi From Ness");
		String a = new String("aaa");
		String c = "ccc";
		
		// operators + += == !=
		//s += a;
		
		//System.out.println(s);
		
		//substring
		String sub = s.substring(0, s.length());
		System.out.println(sub);
		
		
		//concat
		s = s.concat(a);
		System.out.println("S " + s);
		
		//replace replaceFirst replaceAll  
		String s1 = "Bye Bye Bye";
		s1 = s1.replace('y', 'a');
		System.out.println("S1 " + s1);
		//trim and changing case 
		
		String trim1 = "      Pratik    ";
		System.out.println(trim1.trim());
		
		//** comparison 
		// == ref
		// equlas --> data 
		
		String r1 = "ABC";
		String r2 = "ABC";
		String r3 = new String("ABC");
		String r4 = new String("ABC");
		
		/*System.out.println(r3 == r4);
		System.out.println(r3.equals(r4));*/
		
		//startsWith and endsWith
		boolean rs =  s.startsWith("Hi");
		System.out.println(rs);
		
		//indexOf and charAt
		System.out.println(r1.indexOf("B"));
		System.out.println(r1.charAt(2));
		
	}

}
