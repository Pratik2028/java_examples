package com.example.functions;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import collection.Employee;
import collection.EmployeeUtils;

public class FunctionsTest {

	public static void main(String[] args) {
		
		//predicateTest();
		
		//consumerTest();
		
		//supplier();
		
		functions();
	}

	private static void functions() {
		
		Function<String, Employee> createEmployeeFunction = (String name) -> new Employee(name);
		
		Employee newJoinner = createEmployeeFunction.apply("Pratik");
		
		System.out.println(newJoinner);
	}

	private static void supplier() {
		Supplier<List<Employee>> getNewlyJoinnedEmployess = () -> EmployeeUtils.getEmployeeList();
		
		List<Employee> empList = getNewlyJoinnedEmployess.get();
	}

	private static void consumerTest() {
		Consumer<Employee> persistEmployee = (emp) -> {
								System.out.println("Saving Employee To DataBase");
								System.out.println("Employee Details are :" + emp);
		};
		
		List<Employee> newJoinners = EmployeeUtils.getEmployeeList();
		
		for (Employee employee : newJoinners) {
			persistEmployee.accept(employee);
		}
	}

	private static void predicateTest() {
		
		List<Employee> empList = EmployeeUtils.getEmployeeList();
		
		Predicate<Employee> isManager = (emp) -> emp.isManager();
		
		List<Employee> managers = empList.stream().filter(isManager).collect(Collectors.toList());
		
		List<Employee> employess = empList.stream().filter(isManager.negate()).collect(Collectors.toList());
		
		System.out.println(employess);
		
	}

}
