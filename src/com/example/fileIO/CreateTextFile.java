package com.example.fileIO;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.List;

import collection.Employee;
import collection.EmployeeUtils;

public class CreateTextFile {

	public static void main(String[] args) {
		try (Formatter output = new Formatter("javaTrainingData.txt")) {

			List<Employee> empList = EmployeeUtils.getEmployeeList();

			empList.forEach(emp -> {
				output.format("%s %s %s %n", emp.getId().toString(),
						emp.getName(), emp.getSalary());
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
