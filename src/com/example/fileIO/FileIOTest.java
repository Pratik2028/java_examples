package com.example.fileIO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
import java.util.stream.Stream;

public class FileIOTest {

	public static void main(String[] args) throws IOException {
		
		/*Path pathToFileIOFolder= Paths.get("c:","FileIO");
		
		//System.out.println(Files.isDirectory(pathToFileIOFolder));
		
		Path pathToNIOFolder = pathToFileIOFolder.resolve("NIO");
		
		System.out.println(Files.exists(pathToNIOFolder));
		
		//resolve siblings 
		Path pathToNIOFolder1 = pathToFileIOFolder.resolveSibling("ATP");
		System.out.println(Files.exists(pathToNIOFolder1));*/
		
		//demoprogram();
		
		//fileManipulation();
		
	}

	private static void fileManipulation() {
		try {
			
			//Files.createDirectory(Paths.get("Java Training"));
			
			//Files.createDirectories(Paths.get("sub1","sub2","sub3"));
			
			//Files.createFile(Paths.get("sub1","sub2","sub3","myText.txt"));
			
			//Files.deleteIfExists(Paths.get("sub1","sub2","sub3","myText.txt"));
			
			//Files.deleteIfExists(Paths.get("sub1","sub2","sub3"));
			
			//Files.deleteIfExists(Paths.get("sub1"));
			
			/*try(Stream<String> line = Files.lines(Paths.get("c:","FileIO","Alice Adventures in Wonderland.txt"))){
				line.forEach(System.out::println);
			}
			
			byte[] byes = Files.readAllBytes(Paths.get("c:","FileIO","Alice Adventures in Wonderland.txt"));
			
			try (FileOutputStream fos = new FileOutputStream("d:\\AAIW.txt")) {
				   fos.write(byes);
			}*/
			
			/*Path c = Paths.get("d:","Ebooks");
			try(Stream<Path> entry = Files.walk(c)){
				entry.forEach(System.out::println);
			}*/
			
			//Files.move and files copy
			Path source = Paths.get("c:","FileIO","Alice Adventures in Wonderland.txt");
			Path target = Paths.get("c:","NIO2","Alice Adventures in Wonderland.txt");
			
			
			//Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
			
			
			Files.move(source, target, StandardCopyOption.REPLACE_EXISTING);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void demoprogram() throws IOException {
		Scanner sc = new Scanner(System.in);

		System.out.println(" Enter File or Directory Name : ");

		Path path = Paths.get(sc.nextLine());

		if (Files.exists(path)) {
			
			System.out.printf("%s exits %n", path.getFileName());

			System.out.printf("%s is directory %s%n", path.getFileName(), Files.isDirectory(path));

			System.out.printf("%s is Absolute path %s%n", path.getFileName(), path.isAbsolute());

			System.out.printf("For %s Absolute path is %s%n", path.getFileName(), path.toAbsolutePath());

			System.out.printf("%s is Last modified at %s%n", path.getFileName(), Files.getLastModifiedTime(path));

			System.out.printf("%s size is  %sKB%n", path.getFileName(), Files.size(path) / 1024);

			if (Files.isDirectory(path)) {
				System.out.println(" Directory Contains ... ");

				DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path);

				directoryStream.forEach(System.out::println);
			}
		} else {
			System.out.printf("%s does not exits %n", path);
		}
	}
}
