package reflection;

import java.lang.reflect.Method;

import collection.Employee;

public class ReflectoinTest {

	public static void main(String[] args) {
		/*
		 * Reflection is a language's ability to inspect and dynamically call classes, 
		 * methods, attributes, etc. at runtime.
		 */
		
		String s1 = new String(); 
		
		Class<?> c = s1.getClass();
		
		
		
		//to check method return time
		Class<?> c1 = void.class;
		
		//class name with package
		System.out.println(c.getName());
		
		
		//only class name
		System.out.println(c.getSimpleName());
		
		String stringClassName = "java.lang.String";
		
		
		try {
			
			Class<?> c2 = Class.forName(stringClassName);
			System.out.println("forName Method o/p: " + c2.getSimpleName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		//superClass
		Class<?> superClass = c.getSuperclass();
		System.out.println("SuperClass Name : " + superClass.getName());
		//System.out.println("SuperClass Name : " + superClass.getSuperclass().getName());
		
		//Employee
		Employee emp = new Employee();
		
		Class<?> classs = emp.getClass();
		
		Method[] method = classs.getDeclaredMethods();
		
		System.out.printf("%s Contains Following Methods : %n", classs.getSimpleName());
		for (Method m1 : method) {
			System.out.println(m1);
		}
		
	}

}
