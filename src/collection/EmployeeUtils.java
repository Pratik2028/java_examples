package collection;

import java.util.ArrayList;
import java.util.List;

public class EmployeeUtils {
	public static List<Employee> getEmployeeList(){
		List<Employee> empList = new ArrayList<Employee>();
		
		Employee e1 = new Employee();
		e1.setId(5L);
		e1.setName("Bhushan");
		e1.setSalary(60000.00);
		e1.setManager(false);
		e1.setRole("SQA");
		empList.add(e1);
		
		Employee e2 = new Employee();
		e2.setId(4L);
		e2.setName("Dipesh");
		e2.setSalary(70000.00);
		e2.setRole("QA");
		empList.add(e2);
		
		Employee e3 = new Employee();
		e3.setId(3L);
		e3.setName("Ajay");
		e3.setSalary(80000.00);
		e3.setRole("MTS");
		empList.add(e3);
		
		Employee e4 = new Employee();
		e4.setId(1L);
		e4.setName("Prashant");
		e4.setRole("MTS");
		e4.setSalary(90000.00);
		empList.add(e4);
		
		Employee e5 = new Employee();
		e5.setId(2L);
		e5.setName("Anuja");
		e5.setRole("QA");
		e5.setSalary(500000.00);
		empList.add(e5);
		
		Employee e6 = new Employee();
		e6.setId(6L);
		e6.setName("Raj");
		e6.setSalary(500000.00);
		e6.setRole("Manager");
		e6.setManager(true);
		empList.add(e6);
		
		return empList;
	}
}
