package collection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionsExample {

	public static void main(String[] args) throws IOException {
		
		//listExample();
		
		//comparableAndComparatorExample();
		
		//setExamples();
		
		//MapExmaples();
		
	}
	
	
	private static void MapExmaples() {
		// why and when to use map 
		
		List<Employee> empList = EmployeeUtils.getEmployeeList();
		
		Map<Employee, Double> empMap = new HashMap<Employee, Double>();
		
		//Before JAVA 8
		for (Employee employee : empList) {
			empMap.put(employee, employee.getSalary());
		}
		
		System.out.println("");
		
		//HashMap
		Map<Long, String> traineeMap = new HashMap<Long, String>();
		traineeMap.put(3L, "Anuja");
		traineeMap.put(4L, "Prashant");
		traineeMap.put(1L, "Bhushan");
		traineeMap.put(2L, "Dipesh");
		traineeMap.put(5L, "Ajay");

		Set<Long> keys = traineeMap.keySet();
		keys.remove(5L);
		//remove 
		//traineeMap.remove(3L);
		
		for(Long id : traineeMap.keySet() ){
			//System.out.println("Employee Id is  :" + id + " Employee name is: " + traineeMap.get(id));
		}
		
		for(Map.Entry<Long, String> entry : traineeMap.entrySet()){
			//System.out.println(entry.getKey() + " ----> " + entry.getValue());
		}
		// JAVA 8
		//traineeMap.forEach((key, value) -> System.out.println("For Key: " + key + " value is " + value));
		
		/*String[] names = {"Pratik","Dipesh", "Pratik","Bhashan","Ajay","Anuja","Ajay"};
		Map<String, Integer> nameCountMap = new HashMap<String, Integer>();
		for (String name : names) {
			nameCountMap.put(name, nameCountMap.get(name)+1);
		}*/
		
		
		TreeMap<Long, String> SortedEmployeeMap 
					= new TreeMap<Long, String>(traineeMap);
		
		SortedEmployeeMap.
			forEach((id, employee) -> {
				System.out.println("Employee Id: " + id + " "+ employee);
				});
		
	}


	private static void setExamples() {
		Set<String> hs = new HashSet<String>();
		hs.add("Bhushan");
		hs.add("Dipesh");
		hs.add("Ajay");
		hs.add("Prashant");
		hs.add("Anuja");
		hs.add("Anuja");
		System.out.println(hs.add("Prashant"));
		hs.forEach(trainee -> System.out.println(trainee));
		
		System.out.println("<------------------- LinkedHashSet -------------->");
		Set<String> inorderSet = new LinkedHashSet<String>();
		inorderSet.add("Bhushan");
		inorderSet.add("Dipesh");
		inorderSet.add("Ajay");
		inorderSet.add("Prashant");
		inorderSet.add("Anuja");
		inorderSet.forEach(trainee -> System.out.println(trainee));
		
		System.out.println();
		System.out.println("<------------------- TreeSet -------------->");
		Set<String> treeSet = new TreeSet<String>();
		treeSet.add("Bhushan");
		treeSet.add("Dipesh");
		treeSet.add("Ajay");
		treeSet.add("Prashant");
		treeSet.add("Anuja");
		treeSet.forEach(trainee -> System.out.println(trainee));
		
		
		List<String> traineeList = new ArrayList<String>(treeSet);
	}


	private static void comparableAndComparatorExample() {
		
		String[] employeeArray = {"Bhushan","Dipesh","Ajay","Prashant","Anuja"};
		
		System.out.println("<----------- Before Sorting---------------->" );
		for (String name : employeeArray) {
			System.out.println(name);
		}
		//how to sort arrays
		
		Arrays.sort(employeeArray);
		
		int searchresult = Arrays.binarySearch(employeeArray, "Ajay");
		//System.out.println("Search Result :" + searchresult) ;
		
		System.out.println("<----------- After Arrays Sorting---------------->" );
		for (String name : employeeArray) {
			System.out.println(name);
		}
		
		List<String> employeeList1 = Arrays.asList(employeeArray);
		//employeeList1.add("ABC");
		
		Collections.sort(employeeList1, new StringLengthComp());
		System.out.println("<----------- String length sorting ---------------->" );
		employeeList1.forEach(name -> System.out.println(name));
		//HOW TO SORT LIST
		
		List<Employee> employeeList2 = EmployeeUtils.getEmployeeList();
		System.out.println("<----------- Before Sorting---------------->" );
		employeeList2.forEach(emp -> System.out.println(emp));
		
		Collections.sort(employeeList2);
		System.out.println("<----------- After Sorting---------------->" );
		employeeList2.forEach(emp -> System.out.println(emp));
		
		//how to sort by salary
		Collections.sort(employeeList2, new SalaryComparator());
		System.out.println("<----------- salary Sorting---------------->" );
		employeeList2.forEach(emp -> System.out.println(emp));
		
		//here we are using comparator new static methods
		List<Employee> employeeList3 = EmployeeUtils.getEmployeeList();
		System.out.println("<----------- Before Sorting---------------->" );
		employeeList3.forEach(emp -> System.out.println(emp));
		System.out.println("<----------- After Sorting---------------->" );
		Collections.sort(employeeList3, new SalaryComparator().thenComparing((e1,e2) -> e1.getName().compareTo(e2.getName())));
		employeeList3.forEach(emp -> System.out.println(emp));
		//sorting Employees By Comparing method
		Collections.sort(employeeList3, Comparator.comparing(Employee::getName));
	}


	private static void listExample() {
		Employee[] empArray = new Employee[10];
		
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		
		empArray[0] = e1;
		empArray[1] = e2;
		
		//Generics
		List employeeList = EmployeeUtils.getEmployeeList();
		
		
		/*for(int i=0 ; i<employeeList.size() ; i++){
			Employee e = employeeList.get(i);
			employeeList.remove(i);
			System.out.println(e);
		}*/
		
		
		
		
		Iterator<Employee> it = employeeList.iterator();
		
		
		while (it.hasNext()) {
			Employee employee = (Employee) it.next();
			it.remove();
			System.out.println(employee);
		}
		
	}


	
}
